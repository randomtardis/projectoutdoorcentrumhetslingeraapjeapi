<?php

namespace App\Http\Controllers;

use App\Cursus;
use App\Factuur;
use App\FactuurRegel;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Session\Session;

class FactuurRegelController extends Controller
{

//    public $userId;
//
//    public function __construct()
//    {
//        $this->userId = session('user.id');
//    }

    public function store(Request $request)
    {
        $factuurregel = new \App\FactuurRegel;
        $factuurregel->cursusId=$request->get('cursusId');
        $factuurregel->factuurId=$request->get('factuurId');

        $factuurregel->save();

        return json_encode(['message' => 'success']);
    }

    public function update(Request $request, $id){
        $factuurregel = factuurregel::findOrFail($id);
        $factuurregel->cursusId=$request->get('cursusId');
        $factuurregel->factuurId=$request->get('factuurId');
        $factuurregel->save();

        return json_encode(['message' => 'success']);
    }

    public function destroy($id){
        $factuurregel = factuurregel::findOrFail($id);
        $factuurregel->delete();

        return json_encode(['message' => 'success']);
    }

    public function checkFactuur (Request $request)
    {
        $facturen = Factuur::all();
        $cursus = Cursus::findOrFail($request->get('cursusId'));
        $factuurRegels = FactuurRegel::all();
        $date = new DateTime($cursus->datum ) ;
        $userId =  session('user.id');
        $factuurDate = $date->modify('first day of next month')->format('Y-m-d');
        $userFacturen = array();

        foreach ($facturen as $factuur){
            if ($factuur->userId == $userId){
                $userFacturen[] = $factuur;

            }
        }



        if (!$userFacturen) {
            $factuur = new Factuur();
            $factuur->userId= $userId;
            $factuur->date=$factuurDate;
            $factuur->save();

            //de nieuwste factuur_id
            $newFactuurId = DB::getPdo()->lastInsertId();

            //FIX factuur regel to factuur
            $factuurregel = new FactuurRegel();
            $factuurregel->cursusId=$request->get('cursusId');
            $factuurregel->factuurId= $newFactuurId;

            $factuurregel->save();

            $request->session()->flash('status', 'inschrijven voltooid');

            return redirect()->route('getKlimmen');
        }

        if (count($userFacturen)>0) {
            foreach ($userFacturen as $userFactuur){
                if ($userFactuur->date === $factuurDate) {

                    foreach ($facturen as $factuur){
                        foreach ($factuurRegels as $factuurRegel) {

                            if ($factuur->userId == $userId && $factuur->date == $factuurDate) {

                                if ($factuurRegel->factuurId == $factuur->factuurId) {
                                    if ($factuurRegel->cursusId == (int)$request->get('cursusId')) {
                                        $request->session()->flash('status', 'U bent al ingeschreven');

                                        return redirect()->route('getKlimmen');
                                    }

                                    $factuurregel = new FactuurRegel();
                                    $factuurregel->cursusId = $request->get('cursusId');
                                    $factuurregel->factuurId = $factuur->factuurId;

                                    $factuurregel->save();
                                }
                                elseif($factuurRegel->factuurId != $factuur->factuurId || $factuurRegel->cursusId != (int)$request->get('cursusId')) {
                                    //komt altijd hier

                                    printf(" Dit doet eigenlijk helemaal niks, maar goed je kijkt hier toch niet");
                                }
                            }elseif($factuur->userId != $userId || $factuur->date != $factuurDate){
                                //komt altijd hier
                                printf(" G*dverbleeeeep ik wil dat deze kut check werkt. 
                                If it looks stupid but it works, it ain't stupid");
                            }
                        }
                    }
                    $request->session()->flash('status', 'inschrijven voltooid');

                    return redirect()->route('getKlimmen');
                }

                $factuur = new Factuur();
                $factuur->userId= $userId;
                $factuur->date=$factuurDate;
                $factuur->save();

                $newFactuurId = DB::getPdo()->lastInsertId();

                //FIX factuur regel to factuur
                $factuurregel = new FactuurRegel();
                $factuurregel->cursusId=$request->get('cursusId');
                $factuurregel->factuurId= $newFactuurId;

                $factuurregel->save();

                $request->session()->flash('status', 'failed');
            }

            $request->session()->flash('status', 'inschrijven voltooid');

            return redirect()->route('getKlimmen');
        }

        $request->session()->flash('status', 'inschrijven mislukt');

        return redirect()->route('getKlimmen');

    }

}
