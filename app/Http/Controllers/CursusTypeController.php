<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CursusType;

class CursusTypeController extends Controller
{
    public function index(){
        $cursusTypes = CursusType::all();
        return view('admin.allCursusTypes', compact('cursusTypes'));
    }

    public function store(Request $request){
        $cursus = new CursusType;
        $cursus->naam = $request->get('naam');
        $cursus->prijs = $request->get('prijs');
        $cursus->maxAantal = $request->get('maxAantal');

        $cursus->save();

        return json_encode(['message' => 'success']);
    }

    public function show($id){
        $cursusType = CursusType::findOrFail($id);

        return view('base.singleCursusType',compact('cursusType'));
    }

    public function update($id){
        $cursusTypes = CursusType::findOrFail($id);
        return view('admin.updateCursusTypes',compact('cursusTypes'));
    }

    public function updateCursusType(Request $request, $id){
        $cursusType = CursusType::findOrFail($id);
        $cursusType->naam=$request->get('naam');
        $cursusType->prijs=$request->get('prijs') * 100;
        $cursusType->maxAantal=$request->get('maxAantal');
        $cursusType->save();

        $cursusTypes = CursusType::all();

        return view('admin.allCursusTypes', compact('cursusTypes'));
    }




    public function destroy($id){
        $cursus = CursusType::findOrFail($id);
        $cursus->delete();

        return json_encode(['message' => 'success']);
    }
}
