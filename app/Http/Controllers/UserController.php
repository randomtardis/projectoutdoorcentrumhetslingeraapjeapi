<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use App\User;
use SebastianBergmann\Diff\Output\DiffOnlyOutputBuilder;
use function Sodium\compare;

/**
 * @property  SessionController
 */
class UserController extends Controller
{

    public function logOut(){
        $this->setLogedUser(' ', 0 , 0);

        return view(' base.start')->with('status', 'User logged out!');
    }

    public function setLogedUser($email, $rol, $id) {
        session(['user' => ['$email'=> $email,'rol'=>$rol, 'id'=>$id]]);
    }

    public function loginForm(){
        return view('auth.login')->with('status', 'User created!');
    }

    public function instPage()
    {
        return view('teacher.inst')->with('status', 'Login succes!');
    }

    public function login(Request $request){

        //
        $user = DB::select('select * from users where email = ?', [$request->get('email')]);
        if(empty($user)){
            $this->setLogedUser(' ', 0 , 0);
            return view('base.start')->with(session('status', 'User not found'));
        } else {
            if (password_verify($request->get('Wachtwoord'), $user[0]->wachtwoord)) {
                if ($user[0]->rol == 1) {

                    $this->setLogedUser($user[0]->email, $user[0]->rol, $user[0]->id);


                    return view('user.userPage')->with(session('status', 'Login succes!'));
                } elseif ($user[0]->rol == 2) {

                    $this->setLogedUser($user[0]->email, $user[0]->rol, $user[0]->id);

                    return view('teacher.instPage', compact('user'))->with(session('status', 'Login succes!'));
                } elseif ($user[0]->rol == 3){

                    $this->setLogedUser($user[0]->email, $user[0]->rol, $user[0]->id);

                    return view('admin.adminPage', compact('user'))->with(session('status', 'Login succes!'));
                }



            } else {

                return view('base.start')->with(session('status', 'Password incorrect'));
            }
        }
    }
//  inst

    public function storeInst(Request $request)
    {
        $user = new \App\User;
        $user->voornaam=$request->get('voornaam');
        $user->tussenvoegsel=$request->get('tussenvoegsel');
        $user->achternaam=$request->get('achternaam');
        $user->geboortedatum = $request->get('geboortedatum');
        $user->woonplaats=$request->get('woonplaats');
        $user->telefoon=$request->get('telefoon');
        $user->postcode=$request->get('postcode');
        $user->adres=$request->get('adres');
        $user->rol=$request->get('rol');
        $user->email=$request->get('email');
        $user->wachtwoord=bcrypt($request->get('wachtwoord'));
        $user->save();


        //redirect

        return view('teacher.instinfo',compact('user'));
    }


    public function showInst($id){
        $user = User::findOrFail($id);
        return view('teacher.instinfo',compact('user'));
        //return view(' ' ,compact(''));
    }

    public function updateInst($id){
        $user = User::findOrFail($id);
        return view('teacher.updateInst',compact('user'));
    }

    public function updateInstUser($id, Request $request){
        $user = User::findOrFail($id);
        $user->voornaam = $request->get('voornaam');
        $user->tussenvoegsel=$request->get('tussenvoegsel');
        $user->achternaam=$request->get('achternaam');
        $user->woonplaats=$request->get('woonplaats');
        $user->telefoon=$request->get('telefoon');
        $user->postcode=$request->get('postcode');
        $user->adres=$request->get('adres');
        $user->rol=$request->get('rol');
        $user->email=$request->get('email');
        $user->wachtwoord=$request->get('wachtwoord');
        $user->save();

        return view('teacher.instinfo',compact('user'));
        //redirect
    }

    public function destroyInst($id){
        $user = User::findOrFail($id);
        $user->delete();

        $users = User::all();

        return view('teacher.allUsers',compact('users'));
        //redirect
    }

    //  user
    public function create(){
        return view('user.createuser');
    }
    public function store(Request $request)
    {
        $user = new \App\User;
        $user->voornaam=$request->get('voornaam');
        $user->tussenvoegsel=$request->get('tussenvoegsel');
        $user->achternaam=$request->get('achternaam');
        $user->geboortedatum = $request->get('geboortedatum');
        $user->woonplaats=$request->get('woonplaats');
        $user->telefoon=$request->get('telefoon');
        $user->postcode=$request->get('postcode');
        $user->adres=$request->get('adres');
        $user->rol=$request->get('rol');
        $user->email=$request->get('email');
        $user->wachtwoord=bcrypt($request->get('wachtwoord'));
        $user->save();


        //redirect

        return view('user.userinfo',compact('user'));
    }


    public function getUser($id){
        $user = User::findOrFail($id);
        return view('user.userinfo',compact('user'));
        //return view(' ' ,compact(''));
    }

    public function update($id){
        $user = User::findOrFail($id);
        return view('user.updateUser',compact('user'));
    }

    public function updateUser($id, Request $request){
        $user = User::find($id);
        $user->voornaam = $request->get('voornaam');
        $user->tussenvoegsel=$request->get('tussenvoegsel');
        $user->achternaam=$request->get('achternaam');
        $user->woonplaats=$request->get('woonplaats');
        $user->telefoon=$request->get('telefoon');
        $user->postcode=$request->get('postcode');
        $user->adres=$request->get('adres');
        $user->rol=$request->get('rol');
        $user->email=$request->get('email');
        $user->wachtwoord=$request->get('wachtwoord');
        $user->save();

        return view('user.userinfo',compact('user'));
        //redirect
    }

    public function destroy($id){
        $user = User::findOrFail($id);
        $user->delete();

        $users = User::all();

        return view('user.allUsers',compact('users'));
        //redirect
    }

    //All admin functions

    public function indexAdmin(){
        $users = User::all();

        return view('admin.allUsers',compact('users'));
    }

    public function createAdmin(){
        return view('admin.createuser');
    }
    public function storeAdmin(Request $request)
    {
        $user = new \App\User;
        $user->voornaam=$request->get('voornaam');
        $user->tussenvoegsel=$request->get('tussenvoegsel');
        $user->achternaam=$request->get('achternaam');
        $user->geboortedatum = $request->get('geboortedatum');
        $user->woonplaats=$request->get('woonplaats');
        $user->telefoon=$request->get('telefoon');
        $user->postcode=$request->get('postcode');
        $user->adres=$request->get('adres');
        $user->rol=$request->get('rol');
        $user->email=$request->get('email');
        $user->wachtwoord=bcrypt($request->get('wachtwoord'));
        $user->save();


        //redirect

        return view('admin.singleUser',compact('user'));
    }


    public function showAdmin($id){
        $user = User::findOrFail($id);
        return view('admin.singleUser',compact('user'));
    }

    public function updateAdmin($id){
            $user = User::findOrFail($id);
            return view('admin.updateUser',compact('user'));
    }

    public function updateAdminUser($id, Request $request){
        $user = User::find($id);
        $user->voornaam = $request->get('voornaam');
        $user->tussenvoegsel=$request->get('tussenvoegsel');
        $user->achternaam=$request->get('achternaam');
        $user->woonplaats=$request->get('woonplaats');
        $user->telefoon=$request->get('telefoon');
        $user->postcode=$request->get('postcode');
        $user->adres=$request->get('adres');
        $user->rol=$request->get('rol');
        $user->email=$request->get('email');
        $user->wachtwoord=$request->get('wachtwoord');
        $user->save();

        $users = User::all();

        return view('admin.allUsers',compact('users'));
        //redirect
    }

    public function destroyAdmin($id){
        $user = User::findOrFail($id);
        $user->delete();

        $users = User::all();

        return view('admin.allUsers',compact('users'));
        //redirect
    }
}
