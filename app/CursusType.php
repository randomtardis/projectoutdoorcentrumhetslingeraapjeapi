<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CursusType extends Model
{
    protected $table = 'cursustype';

    public function Cursus(){
        return $this->hasMany('App\Cursus', 'cursusTypeId');
    }
}
