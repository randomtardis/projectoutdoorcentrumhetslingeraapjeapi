@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Gegevens bijwerken') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('updateInstUser', ['id' => session('user->id')]) }}">
                            @csrf

                            <div class="form-group row">
                                <label for="voornaam" class="col-md-4 col-form-label text-md-right">{{ __('voornaam') }}</label>

                                <div class="col-md-6">
                                    <input id="voornaam" type="text" class="form-control{{ $errors->has('voornaam') ? ' is-invalid' : '' }}" name="voornaam" value="{{ $user->voornaam }}" required autofocus>

                                    @if ($errors->has('voornaam'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('voornaam') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="tussenvoegsel" class="col-md-4 col-form-label text-md-right">{{ __('tussenvoegsel') }}</label>

                                <div class="col-md-6">
                                    <input id="tussenvoegsel" type="text" class="form-control{{ $errors->has('tussenvoegsel') ? ' is-invalid' : '' }}" name="tussenvoegsel" value="{{ $user->tussenvoegsel }}"  autofocus>

                                    @if ($errors->has('tussenvoegsel'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tussenvoegsel') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="achternaam" class="col-md-4 col-form-label text-md-right">{{ __('achternaam') }}</label>

                                <div class="col-md-6">
                                    <input id="achternaam" type="text" class="form-control{{ $errors->has('achternaam') ? ' is-invalid' : '' }}" name="achternaam" value="{{ $user->achternaam }}" required autofocus>

                                    @if ($errors->has('achternaam'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('achternaam') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="geboortedatum" class="col-md-4 col-form-label text-md-right">{{ __('geboortedatum') }}</label>

                                <div class="col-md-6">
                                    <input id="geboortedatum" type="date" class="form-control{{ $errors->has('geboortedatum') ? ' is-invalid' : '' }}" name="geboortedatum" value="{{ $user->geboortedatum }}" required autofocus>

                                    @if ($errors->has('geboortedatum'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('geboortedatum') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="woonplaats" class="col-md-4 col-form-label text-md-right">{{ __('woonplaats') }}</label>

                                <div class="col-md-6">
                                    <input id="woonplaats" type="text" class="form-control{{ $errors->has('woonplaats') ? ' is-invalid' : '' }}" name="woonplaats" value="{{ $user->woonplaats }}" required autofocus>

                                    @if ($errors->has('woonplaats'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('woonplaats') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="telefoon" class="col-md-4 col-form-label text-md-right">{{ __('telefoon') }}</label>

                                <div class="col-md-6">
                                    <input id="telefoon" type="tel" max="9999999999" class="form-control{{ $errors->has('telefoon') ? ' is-invalid' : '' }}" name="telefoon" value="{{ $user->telefoon }}" required autofocus>

                                    @if ($errors->has('telefoon'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('telefoon') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="postcode" class="col-md-4 col-form-label text-md-right">{{ __('postcode') }}</label>

                                <div class="col-md-6">
                                    <input id="postcode" type="text" class="form-control{{ $errors->has('postcode') ? ' is-invalid' : '' }}" name="postcode" value="{{ $user->postcode }}" required autofocus>

                                    @if ($errors->has('postcode'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('postcode') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="adres" class="col-md-4 col-form-label text-md-right">{{ __('adres') }}</label>

                                <div class="col-md-6">
                                    <input id="adres" type="text" class="form-control{{ $errors->has('adres') ? ' is-invalid' : '' }}" name="adres" value="{{ $user->adres }}" required autofocus>

                                    @if ($errors->has('adres'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('adres') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-md-6">
                                    <input  type="hidden" class="form-control{{ $errors->has('rol') ? ' is-invalid' : '' }}" name="rol" value="1" >

                                    @if ($errors->has('rol'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('rol') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Bijwerken') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
