@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Mijn gegevens') }}</div>

                    <div class="card-body">
                        {{$user->voornaam}}
                        <ul>
                            @if($user->tussenvoegsel === '')
                                <li>
                                    {{$user->tussenvoegsel}}
                                </li>
                            @endif


                            <li>
                                {{$user->achternaam}}
                            </li>
                            <li>
                                {{$user->geboortedatum}}
                            </li>
                            <li>
                                {{$user->woonplaats}}
                            </li>
                            <li>
                                {{$user->telefoon}}
                            </li>
                            <li>
                                {{$user->postcode}}
                            </li>
                            <li>
                                {{$user->adres}}
                            </li>
                            <li>
                                {{$user->email}}
                            </li>
                        </ul>
                        @if($user->rol != NULL)
                            <a class="nav-link" href="{{ route('updateInst', ['id' => session('user.id')])  }}">{{ __('Gebruiker bijwerken') }}</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
