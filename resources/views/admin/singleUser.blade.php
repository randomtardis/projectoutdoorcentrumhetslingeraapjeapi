@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('New User') }}</div>

                    <div class="card-body">
                            {{$user->voornaam}}
                            <ul>
                                @if($user->tussenvoegsel === '')
                                    <li>
                                        {{$user->tussenvoegsel}}
                                    </li>
                                @endif


                                <li>
                                    {{$user->achternaam}}
                                </li>
                                <li>
                                    {{$user->geboortedatum}}
                                </li>
                                <li>
                                    {{$user->woonplaats}}
                                </li>
                                <li>
                                    {{$user->telefoon}}
                                </li>
                                <li>
                                    {{$user->postcode}}
                                </li>
                                <li>
                                    {{$user->adres}}
                                </li>
                                <li>
                                    {{$user->email}}
                                </li>
                                @if($user->rol == 1)
                                    <li>
                                        Gebruiker
                                    </li>
                                @elseif($user->rol == 2)
                                    <li>
                                        Instructeur
                                    </li>
                                @elseif($user->rol == 3)
                                    <li>
                                        Admin
                                    </li>
                                @endif

                            </ul>
                        <a class="nav-link" href="{{ route('indexAdmin') }}">{{ __('All Users') }}</a>
                        @if($user->rol != NULL)
                            <a class="nav-link" href="{{ route('updateAdmin', ['id' => $user->id]) }}">{{ __('Update User') }}</a>
                        @endif
                        @if($user->rol != NULL)
                            <a class="nav-link" href="{{ route('destroyAdmin', ['id' => $user->id]) }}">{{ __('Delete User') }}</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
