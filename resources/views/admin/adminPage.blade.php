@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <h1>Mijn pagina</h1>
    </div>
    <div class="row">
        @if (session('status'))
            <div class="alert alert-warning" role="alert">
                {{ session('status') }}
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-md-8">
            <ul>
                <li>
                    <a class="nav-link" href="{{ route('indexAdmin') }}">{{ __('Alle gebruikers') }}</a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('getCursusses') }}">{{ __('Alle cursussen') }}</a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('getCursusTypes') }}">{{ __('Alle cursus types') }}</a>
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection
