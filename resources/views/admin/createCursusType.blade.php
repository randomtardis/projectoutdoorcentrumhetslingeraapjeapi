@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('New Cursus Type') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('newCursusType') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="naam" class="col-md-4 col-form-label text-md-right">{{ __('naam') }}</label>

                                <div class="col-md-6">
                                    <input id="voornaam" type="text" class="form-control{{ $errors->has('naam') ? ' is-invalid' : '' }}" name="naam" value="{{ old('naam') }}" required autofocus>

                                    @if ($errors->has('naam'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('naam') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prijs" class="col-md-4 col-form-label text-md-right">{{ __('prijs') }}</label>

                                <div class="col-md-6">
                                    <input id="prijs" type="number" class="form-control{{ $errors->has('prijs') ? ' is-invalid' : '' }}" name="prijs" value="{{ old('prijs') }}" required autofocus>

                                    @if ($errors->has('prijs'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('prijs') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="maxAantal" class="col-md-4 col-form-label text-md-right">{{ __('maxAantal') }}</label>

                                <div class="col-md-6">
                                    <input id="maxAantal" type="number" class="form-control{{ $errors->has('maxAantal') ? ' is-invalid' : '' }}" name="maxAantal" value="{{ old('maxAantal') }}" required autofocus>

                                    @if ($errors->has('maxAantal'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('maxAantal') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>maxAantal
    </div>
@endsection
