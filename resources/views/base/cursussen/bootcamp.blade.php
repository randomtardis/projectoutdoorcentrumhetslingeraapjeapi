@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h1>Bootcamp</h1>

    </div>

    <div class="row">
        <div class="col-md-7">
            <h3>Wat is Bootcamp?</h3>
            <p>Bootcamp is in groepsverband sporten in de buitenlucht onder begeleiding van een trainer.
                Is binnen in een sportschool zitten niks voor jou? Maar wil je fit, sterk, soepel, strak en in de
                buitenlucht in beweging zijn? Dan is Bootcamp echt iets voor jou! De Bootcamp Team trainingen bestaan
                o.a. uit: hardlopen, intervaltraining en fitnessoefeningen in de natuur. De ideale work-out om je sterk,
                strak, slank en energiek te laten voelen in de uitdagende omgeving van strand, bos, park of duinen
            </p>

            <h3>Kenmerken bootcamp training</h3>
            De belangrijkste kenmerken voor een Bootcamp Team training zijn:
            <ul>
                <li>
                    Sporten met plezier
                </li>
                <li>
                    Actief bewegen in de buitenlucht
                </li>
                <li>
                    Uitdagende omgeving
                </li>
                <li>
                    In groepsverband je conditie en je (spier)kracht verbeteren
                </li>
                <li>
                    Fit en soepel worden (of blijven)
                </li>
                <li>
                    Op je eigen niveau trainen
                </li>
                <li>
                    Afwisselende en uitdagende trainingen o.l.v. een ervaren en professionele trainer\\
                </li>
            </ul>

        </div>
        <div class="col-md-5">
            <div class="jumbotron">
                <h3>Intresse?</h3>
                <p>
                    Schrijf je in door een datum en tijd te kiezen en klik op Inschrijven
                </p>

                <ul>
                    <li>
                        @foreach($cursusTypes as $cursusType)
                            @if($cursusType->id === 4)
                                Kosten: &euro;{{$cursusType->prijs / 100}}
                            @endif
                        @endforeach
                    </li>
                    <li>
                        @foreach($cursusTypes as $cursusType)
                            @if($cursusType->id === 4)
                                Max aantal deelnemers: {{$cursusType->maxAantal}}
                            @endif
                        @endforeach
                    </li>
                    <li>
                        Dag(en) en Tijd(en):
                    </li>
                    <ul>
                        <li>
                            Dag(en):
                        </li>
                        <ul>
                            <li>
                                Maandag
                            </li>
                            <li>
                                Dinsdag
                            </li>
                            <li>
                                Woensdag
                            </li>
                            <li>
                                Donderdag
                            </li>
                            <li>
                                Vrijdag
                            </li>
                        </ul>
                        <li>
                            Tijd(en):
                        </li>
                        <ul>
                            <li>
                                Van 20:00 tot 21:30
                            </li>
                        </ul>
                    </ul>
                </ul>
                @if (session('status'))
                    <div class="alert alert-warning" role="alert">
                        {{ session('status') }}
                    </div>
                @endif<br>
                <form method="POST" action="{{ route('inschrijvenCursus') }}">
                    @csrf
                    <div class="form-group row">
                        <label for="cursusId" class="col-md-4 col-form-label text-md-right">{{ __('cursus') }}</label>
                        <div class="col-md-6">
                            {{--<input  type="" class="form-control{{ $errors->has('rol') ? ' is-invalid' : '' }}" name="rol" value="1" >--}}

                            <select id="cursusId" name="cursusId" class="form-control{{ $errors->has('cursusId') ? ' is-invalid' : '' }}" required autofocus>

                                @foreach($cursusses as $cursus)
                                    @if($cursus->cursusTypeId === 4)
                                        <option value="{{$cursus->id}}">{{$cursus->datum}} {{$cursus->tijdstip}} </option>
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('cursusId'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cursusId') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>



                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-lg btn-primary">
                                {{ __('Inschrijven') }}
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
