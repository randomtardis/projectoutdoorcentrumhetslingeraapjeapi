@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <a href="#" class="btn btn-primary" onclick="printDiv('printable')">Print</a>
        <div id="printable">
            <div class="row justify-content-center">
                <h1>Alle cursussen</h1>
                <a class="nav-link" href="{{ route('newCursus') }}">Nieuwe Cursus</a>


                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>CursusType</th>
                        <th>max deelnemer aantal</th>
                        <th>instructeur</th>
                        <th>opmerking</th>
                        <th>datum</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cursusses as $cursus)
                        <tr>
                            <td>
                                <a class="nav-link" href="{{ route('updateForm', ['id' => $cursus->id]) }}">{{$cursus->id}}</a>
                            </td>
                            <td>
                                @foreach($data[0] as $cursusType)
                                    @if($cursus->cursusTypeId === $cursusType->id)
                                        {{$cursusType->naam}}
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                {{$cursus->maxAantal}}
                            </td>
                            <td>
                                @foreach($data[1] as $user)
                                   @if($user->id == $cursus->instructeur)
                                       {{$user->voornaam}} {{$user->tussenvoegsel}} {{$user->achternaam}}
                                   @endif
                                @endforeach
                            </td>
                            <td>
                                {{$cursus->opmerking }}
                            </td>
                            <td>
                                {{$cursus->datum }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script >
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endsection
