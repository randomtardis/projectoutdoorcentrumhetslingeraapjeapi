@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Gegevens bijwerken') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('updateCursus', ['id' => $cursus->id]) }}">
                            @csrf

                            <div class="form-group row">
                                <label for="cursusTypeId" class="col-md-4 col-form-label text-md-right">{{ __('Cursus type') }}</label>
                                <div class="col-md-6">
                                    {{--<input  type="" class="form-control{{ $errors->has('rol') ? ' is-invalid' : '' }}" name="rol" value="1" >--}}

                                    <select id="cursusTypeId" name="cursusTypeId" class="form-control{{ $errors->has('cursusTypeId') ? ' is-invalid' : '' }}" required autofocus>
                                        @foreach($data[0] as $cursusType)
                                                <option value="{{$cursusType->id}}">{{$cursusType->naam}} </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('cursusTypeId'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cursusTypeId') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="rol" class="col-md-4 col-form-label text-md-right">{{ __('Instructeur') }}</label>
                                <div class="col-md-6">
                                    {{--<input  type="" class="form-control{{ $errors->has('rol') ? ' is-invalid' : '' }}" name="rol" value="1" >--}}

                                    <select id="instructeur" name="instructeur" class="form-control{{ $errors->has('instructeur') ? ' is-invalid' : '' }}" required autofocus>
                                       @foreach($data[1] as $user)
                                           @if($user->rol === 2)
                                                <option value="{{$user->id}}">{{$user->voornaam}} {{$user->tussenvoegsel}} {{$user->achternaam}}</option>
                                           @endif
                                        @endforeach
                                    </select>
                                    @if ($errors->has('instructeur'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('instructeur') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="opmerking" class="col-md-4 col-form-label text-md-right">{{ __('Opmerking') }}</label>

                                <div class="col-md-6">
                                    <input id="opmerking" type="text" class="form-control{{ $errors->has('opmerking') ? ' is-invalid' : '' }}" name="opmerking" value="{{ old('opmerking') }}"  autofocus>

                                    @if ($errors->has('opmerking'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('opmerking') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="maxAantal" class="col-md-4 col-form-label text-md-right">{{ __('max aantal') }}</label>

                                <div class="col-md-6">
                                    @foreach($data[0] as $cursusType)
                                        @if($cursus->cursusTypeId === $cursusType->id)
                                            <input id="maxAantal" type="number" class="form-control{{ $errors->has('maxAantal') ? ' is-invalid' : '' }}" name="maxAantal" value="{{ $cursusType->maxAantal }}"  autofocus>
                                        @endif
                                    @endforeach
                                    @if ($errors->has('maxAantal'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('maxAantal') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="datum" class="col-md-4 col-form-label text-md-right">{{ __('Datum') }}</label>

                                <div class="col-md-6">
                                    <input id="datum" type="date" class="form-control{{ $errors->has('datum') ? ' is-invalid' : '' }}" name="datum" value="{{ $cursus->datum }}" required autofocus>

                                    @if ($errors->has('datum'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('datum') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="tijdstip" class="col-md-4 col-form-label text-md-right">{{ __('Tijdstip') }}</label>

                                <div class="col-md-6">
                                    <input id="tijdstip" type="time" class="form-control{{ $errors->has('tijdstip') ? ' is-invalid' : '' }}" name="tijdstip" value="{{ $cursus->tijdstip }}" required autofocus>

                                    @if ($errors->has('tijdstip'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tijdstip') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Bijwerken') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
