<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('Het Slingeraapje', 'Het Slingeraapje') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('contact') }}">{{ __('Contact') }}</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Ons aanbod
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="nav-link" href="{{ route('getBootcamp') }}">{{ __('Bootcamp') }}</a>
                                <a class="nav-link" href="{{ route('getKanoen') }}">{{ __('Kanoën') }}</a>
                                <a class="nav-link" href="{{ route('getKlimmen') }}">{{ __('Klimmen') }}</a>
                                <a class="nav-link" href="{{ route('getMountainbike') }}">{{ __('Mountainbike') }}</a>
                            </div>
                        </li>

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                    @if(session('user.rol') === 1 || session('user.rol') === 2 || session('user.rol') === 3)
                        @if(session('user.rol') === 1)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('userPage') }}">{{ __('Mijn pagina') }}</a>
                            </li>
                        @elseif(session('user.rol') === 2)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('instPage') }}">{{ __('Mijn pagina') }}</a>
                            </li>
                        @elseif(session('user.rol') === 3)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('adminPage') }}">{{ __('Mijn pagina') }}</a>
                            </li>
                        @endif
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logOut') }}">{{ __('logout') }}</a>
                            </li>
                        @elseif(session('user.rol') === 0 || session('user.id') === null)
                        <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else

                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logOut') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                    </ul>
                    @endif

                </div>
            </div>
        </nav>

        <div id="demo" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
                <li data-target="#demo" data-slide-to="3"></li>
            </ul>

            <!-- The slideshow -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="http://barocka.pl/wp-content/uploads/2017/08/barocka_scianki_wspinaczkowe_slider_001_v2.jpg" alt="Klimmen">
                </div>
                <div class="carousel-item">
                    <img src="https://farmbikes.nl/Farm/uploads/2015/06/FarmBikes-FatLab-fatBike-94154913.jpg" alt="mountainbike">
                </div>
                <div class="carousel-item">
                    <img src="http://traininglab.orphdigital5.com/wp-content/uploads/2018/07/tlnyc2.jpg" alt="bootcamp">
                </div>
                <div class="carousel-item">
                    <img src="http://www.algonquincanoe.com/wp-content/uploads/2015/05/slide-d.jpg" alt="kanoen" >
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>



        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
