<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\CursusTypeRepository")
 */
class CursusType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $naam;

    /**
     * @ORM\Column(type="integer")
     */
    private $max_aantal;

    /**
     * @ORM\Column(type="integer")
     */
    private $prijs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cursus", mappedBy="CursusType")
     */
    private $cursuses;

    public function __construct()
    {
        $this->cursuses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNaam(): ?string
    {
        return $this->naam;
    }

    public function setNaam(string $naam): self
    {
        $this->naam = $naam;

        return $this;
    }

    public function getMaxAantal(): ?int
    {
        return $this->max_aantal;
    }

    public function setMaxAantal(int $max_aantal): self
    {
        $this->max_aantal = $max_aantal;

        return $this;
    }

    public function getPrijs(): ?int
    {
        return $this->prijs;
    }

    public function setPrijs(int $prijs): self
    {
        $this->prijs = $prijs;

        return $this;
    }

    /**
     * @return Collection|Cursus[]
     */
    public function getCursuses(): Collection
    {
        return $this->cursuses;
    }

    public function addCursus(Cursus $cursus): self
    {
        if (!$this->cursuses->contains($cursus)) {
            $this->cursuses[] = $cursus;
            $cursus->setCursusType($this);
        }

        return $this;
    }

    public function removeCursus(Cursus $cursus): self
    {
        if ($this->cursuses->contains($cursus)) {
            $this->cursuses->removeElement($cursus);
            // set the owning side to null (unless already changed)
            if ($cursus->getCursusType() === $this) {
                $cursus->setCursusType(null);
            }
        }

        return $this;
    }
}
