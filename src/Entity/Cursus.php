<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\CursusRepository")
 */
class Cursus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_time;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Instructeur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $opmerking;

    /**
     * @ORM\Column(type="integer")
     */
    private $max_aantal;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FactuurRegel", mappedBy="Cursus_Id")
     */
    private $factuurRegels;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CursusType", inversedBy="cursuses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $CursusType;

    public function __construct()
    {
        $this->factuurRegels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCursusType()
    {
        return $this->CursusType;
    }

    /**
     * @param mixed $CursusType
     */
    public function setCursusType($CursusType): void
    {
        $this->CursusType = $CursusType;
    }

    public function getDateTime(): ?\DateTimeInterface
    {
        return $this->date_time;
    }

    public function setDateTime(\DateTimeInterface $date_time): self
    {
        $this->date_time = $date_time;

        return $this;
    }

    public function getInstructeur(): ?string
    {
        return $this->Instructeur;
    }

    public function setInstructeur(string $Instructeur): self
    {
        $this->Instructeur = $Instructeur;

        return $this;
    }

    public function getOpmerking(): ?string
    {
        return $this->opmerking;
    }

    public function setOpmerking(?string $opmerking): self
    {
        $this->opmerking = $opmerking;

        return $this;
    }

    public function getMaxAantal(): ?int
    {
        return $this->max_aantal;
    }

    public function setMaxAantal(int $max_aantal): self
    {
        $this->max_aantal = $max_aantal;

        return $this;
    }

    /**
     * @return Collection|FactuurRegel[]
     */
    public function getFactuurRegels(): Collection
    {
        return $this->factuurRegels;
    }

    public function addFactuurRegel(FactuurRegel $factuurRegel): self
    {
        if (!$this->factuurRegels->contains($factuurRegel)) {
            $this->factuurRegels[] = $factuurRegel;
            $factuurRegel->setCursusId($this);
        }

        return $this;
    }

    public function removeFactuurRegel(FactuurRegel $factuurRegel): self
    {
        if ($this->factuurRegels->contains($factuurRegel)) {
            $this->factuurRegels->removeElement($factuurRegel);
            // set the owning side to null (unless already changed)
            if ($factuurRegel->getCursusId() === $this) {
                $factuurRegel->setCursusId(null);
            }
        }

        return $this;
    }
}
