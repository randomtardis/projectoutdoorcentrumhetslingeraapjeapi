<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\FactuurRegelRepository")
 */
class FactuurRegel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="factuurRegels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User_Id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Factuur", inversedBy="factuurRegels")
     */
    private $Factuur_Id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cursus", inversedBy="factuurRegels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Cursus_Id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?User
    {
        return $this->User_Id;
    }

    public function setUserId(?User $User_Id): self
    {
        $this->User_Id = $User_Id;

        return $this;
    }

    public function getFactuurId(): ?Factuur
    {
        return $this->Factuur_Id;
    }

    public function setFactuurId(?Factuur $Factuur_Id): self
    {
        $this->Factuur_Id = $Factuur_Id;

        return $this;
    }

    public function getCursusId(): ?Cursus
    {
        return $this->Cursus_Id;
    }

    public function setCursusId(?Cursus $Cursus_Id): self
    {
        $this->Cursus_Id = $Cursus_Id;

        return $this;
    }
}
