<?php

namespace App\Form;

use App\Entity\FactuurRegel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FactuurRegelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('User_Id', EntityType::class, [
                'class' => \App\Entity\User::class,
                'choice_label'  => 'voornaam',
            ])
            ->add('Factuur_Id', EntityType::class, [
                'class' => \App\Entity\Factuur::class,
                'choice_label'  => 'FactuurDatum',
            ])
            ->add('Cursus_Id', EntityType::class, [
                'class' => \App\Entity\Cursus::class,
                'choice_label'  => 'naam',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FactuurRegel::class,
        ]);
    }
}
