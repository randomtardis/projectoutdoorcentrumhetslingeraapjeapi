<?php

namespace App\Form;

use App\Entity\Cursus;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CursusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('cursus_type')
            ->add('date_time')
            ->add('Instructeur')
            ->add('opmerking')
            ->add('max_aantal')
            ->add('CursusType', EntityType::class, [
                'class' => \App\Entity\CursusType::class,
                'choice_label'  => 'naam',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cursus::class,
        ]);
    }
}
