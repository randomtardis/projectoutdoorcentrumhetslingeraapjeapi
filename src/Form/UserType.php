<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Voornaam')
            ->add('tussenvoegsel')
            ->add('achternaam')
            ->add('geboortedatum', DateType::class, array(
                'years' => range(date('Y'), date('Y') -130),
                'required' => true
            ))
            ->add('adres')
            ->add('woonplaats')
            ->add('Telefoon')
            ->add('email')
            ->add('wachtwoord')
            ->add('rol')
            ->add('Postcode')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
