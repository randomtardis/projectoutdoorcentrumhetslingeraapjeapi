<?php

namespace App\Controller;

use App\Entity\CursusType;
use App\Form\CursusType1Type;
use App\Repository\CursusTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cursus_  type")
 */
class CursusTypeController extends AbstractController
{
    /**
     * @Route("/", name="cursus_type_index", methods="GET")
     */
    public function index(CursusTypeRepository $cursusTypeRepository): Response
    {
        return $this->render('cursus_type/index.html.twig', ['cursus_types' => $cursusTypeRepository->findAll()]);
    }

    /**
     * @Route("/new", name="cursus_type_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $cursusType = new CursusType();
        $form = $this->createForm(CursusType1Type::class, $cursusType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cursusType);
            $em->flush();

            return $this->redirectToRoute('cursus_type_index');
        }

        return $this->render('cursus_type/new.html.twig', [
            'cursus_type' => $cursusType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cursus_type_show", methods="GET")
     */
    public function show(CursusType $cursusType): Response
    {
        return $this->render('cursus_type/show.html.twig', ['cursus_type' => $cursusType]);
    }

    /**
     * @Route("/{id}/edit", name="cursus_type_edit", methods="GET|POST")
     */
    public function edit(Request $request, CursusType $cursusType): Response
    {
        $form = $this->createForm(CursusType1Type::class, $cursusType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cursus_type_index', ['id' => $cursusType->getId()]);
        }

        return $this->render('cursus_type/login.html.twig', [
            'cursus_type' => $cursusType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cursus_type_delete", methods="DELETE")
     */
    public function delete(Request $request, CursusType $cursusType): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cursusType->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cursusType);
            $em->flush();
        }

        return $this->redirectToRoute('cursus_type_index');
    }
}
