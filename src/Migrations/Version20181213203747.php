<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181213203747 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, voornaam VARCHAR(255) NOT NULL, tussenvoegsel VARCHAR(255) DEFAULT NULL, achternaam VARCHAR(255) NOT NULL, geboortedatum DATE NOT NULL, adres VARCHAR(255) NOT NULL, woonplaats VARCHAR(255) NOT NULL, telefoon VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, wachtwoord VARCHAR(255) NOT NULL, rol VARCHAR(255) NOT NULL, postcode VARCHAR(6) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cursus DROP cursus_type');
        $this->addSql('ALTER TABLE factuur ADD CONSTRAINT FK_32147710A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE factuur_regel ADD CONSTRAINT FK_62560F599D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE factuur_regel ADD CONSTRAINT FK_62560F5945CCBDB9 FOREIGN KEY (factuur_id_id) REFERENCES factuur (id)');
        $this->addSql('ALTER TABLE factuur_regel ADD CONSTRAINT FK_62560F59ED70AAB9 FOREIGN KEY (cursus_id_id) REFERENCES cursus (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE factuur DROP FOREIGN KEY FK_32147710A76ED395');
        $this->addSql('ALTER TABLE factuur_regel DROP FOREIGN KEY FK_62560F599D86650F');
        $this->addSql('DROP TABLE user');
        $this->addSql('ALTER TABLE cursus ADD cursus_type VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE factuur_regel DROP FOREIGN KEY FK_62560F5945CCBDB9');
        $this->addSql('ALTER TABLE factuur_regel DROP FOREIGN KEY FK_62560F59ED70AAB9');
    }
}
