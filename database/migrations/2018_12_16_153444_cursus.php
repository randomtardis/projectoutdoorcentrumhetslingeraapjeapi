<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cursus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cursusTypeId');
            $table->integer('maxAantal');
            $table->string('instructeur');
            $table->text('opmerking');
            $table->date('datum');
            $table->time('tijdstip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursus');
    }
}
