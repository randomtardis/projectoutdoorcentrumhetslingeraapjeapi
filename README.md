# ProjectOutdoorcentrumHetSlingerAapjeAPI
<h1>
    This is the Repo for A school projects API
</h1>
<p>
    This repo is the api for my school project titled: "Outdoorcentrum Het Slinger Aapje".
    <br>
    This repo exists of an api build on symfony and the Api platform framework.
    <br> 
    For the most stabel version check the Master Branch for the newest version check the Dev Branch.
    <br>
    all docs:
    <br>
    <a href="https://symfony.com/doc/current/index.html#gsc.tab=0">symfony</a>
    <br>
    <a href="https://api-platform.com/docs/">api-platform.com</a>
    <br>
    after installing the project you can find the api docs on /api  
</p>