<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
    return view('base/start');
});

Route::get('/about', function(){
    return view('base.about');
})->name('about');

Route::get('/contact', function(){
    return view('base.contact');
})->name('contact');

Route::get('/bootcamp', 'CursusController@getBootcamp')->name('getBootcamp');
Route::get('/kanoen', 'CursusController@getKanoen')->name('getKanoen');
Route::get('/mountainbike', 'CursusController@getMountainbike')->name('getMountainbike');
Route::get('/klimmen', 'CursusController@getKlimmen')->name('getKlimmen');


Route::get('/userPage', function(){
    return view('user.userPage');
})->name('userPage');

Route::get('/instPage', function(){
    return view('teacher.instPage');
})->name('instPage');

Route::get('/adminPage', function(){
    return view('admin.adminPage');
})->name('adminPage');


Route::get('session/get','SessionController@accessSessionData');
Route::get('session/set','SessionController@storeSessionData');
Route::get('session/remove','SessionController@deleteSessionData');

Route::get('/login','UserController@loginForm')->name('login');
Route::post('/login', 'UserController@login');
Route::get('/logout','UserController@logOut')->name('logOut');
Route::get('/register','UserController@create')->name('register');

Route::get('/user/new','UserController@create')->name('create');
Route::post('/user','UserController@store')->name('store');
Route::get('/getUser/{id}', 'UserController@getUser')->name('getUser');
Route::get('/user/{id}', 'UserController@update')->name('update');
Route::patch('/user/{id}', 'UserController@updateUser')->name('updateUser');
Route::delete('/user/{id}', 'UserController@destroy')->name('destroyUser');

Route::get('/factuurs', 'FactuurController@showFactuursUser')->name('getFactuurs');
Route::get('/factuur/{id}', 'FactuurController@show')->name('showFactuur');
Route::post('/factuur', 'FactuurController@store');

Route::post('/inschrijven', 'FactuurRegelController@checkFactuur')->name('inschrijvenCursus');

Route::get('/inst/user/new','UserController@createInst')->name('createInst');
Route::post('/inst/user','UserController@storeInst')->name('storeInst');
Route::get('/inst/getuser/{id}', 'UserController@showInst')->name('showInst');
Route::get('/inst/user/{id}', 'UserController@updateInst')->name('updateInst');
Route::patch('/inst/user/{id}', 'UserController@updateInstUser')->name('updateInstUser');
Route::delete('/inst/user/{id}', 'UserController@destroyINST')->name('destroyInst');

Route::get('/cursustype', 'CursusTypeController@index')->name('getCursusTypes');
Route::post('/cursustype', 'CursusTypeController@create')->name('create');
Route::post('/cursustype', 'CursusTypeController@store')->name('createCursusType');
Route::get('/cursustype/{id}', 'CursusTypeController@update')->name('update');
Route::post('/cursustype/{id}', 'CursusTypeController@updateCursusType')->name('updateCursusType');


Route::get('/admin/users', 'Usercontroller@indexAdmin')->name('indexAdmin');
Route::get('/admin/user/new','UserController@createAdmin')->name('createAdmin');
Route::post('/admin/user','UserController@storeAdmin')->name('storeAdmin');
Route::get('/admin/getuser/{id}', 'UserController@showAdmin')->name('showAdmin');
Route::get('/admin/user/{id}', 'UserController@updateAdmin')->name('updateAdmin');
Route::patch('/admin/user/{id}', 'UserController@updateAdminUser')->name('updateAdminUser');
Route::delete('/admin/user/{id}', 'UserController@destroyAdmin')->name('destroyAdmin');


//Route::get('/factuurs', 'FactuurController@index');
//Route::post('/factuur', 'FactuurController@store');
//Route::get('/factuur/{id}', 'FactuurController@show');
//Route::delete('/factuur/{id}', 'FactuurController@destroy');
//Route::patch('/factuur/{id}', 'FactuurController@update');
//
//Route::post('/factuurregel', 'FactuurRegelController@store');
//Route::delete('/factuurregel/{id}', 'FactuurRegelController@destroy');
//Route::patch('/factuurregel/{id}', 'FactuurRegelController@update');
//
//Route::post('/cursustype', 'CursusTypeController@store');
//Route::delete('/cursustype/{id}', 'CursusTypeController@destroy');
//Route::patch('/cursustype/{id}', 'CursusTypeController@update');
//
Route::get('/cursussen', 'CursusController@index')->name('getCursusses');
Route::get('/cursus', 'CursusController@newCursus')->name('newCursus');
Route::post('/cursus', 'CursusController@store')->name('safeCursus');
Route::get('/cursus/{id}', 'CursusController@show')->name('getCursus');
Route::get('/cursus/{id}', 'CursusController@updateForm')->name('updateForm');
Route::patch('/cursus/{id}', 'CursusController@updateCursus')->name('updateCursus');
//Route::delete('/cursus/{id}', 'CursusController@destroy');
//Route::patch('/cursus/{id}', 'CursusController@update');

//instructeur
